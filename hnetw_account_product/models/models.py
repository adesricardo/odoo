# -*- coding: utf-8 -*-

from odoo import models, fields, api


class ProductCategory_account_product(models.Model):
     _name = 'product.category'
     _inherit = 'product.category'

     hnetw_compania = fields.Many2one("res.company", string="Nombre Compañia")
     rango_inicial = fields.Integer(string="Rango Inicial", default=1)
     rango_final = fields.Integer(string="Rango Final", default=100)

