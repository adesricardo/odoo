# -*- coding: utf-8 -*-
{
    'name': "hnetw_account_product",

    'summary': """
        Categoria de productos
""",

    'description': """
        Se agregan tres campos a categoria de productos como ser compañia,rango incial,rango final.
    """,

    'author': "HNETW",
    'website': "http://www.hnetw.com",

    'category': 'product,account',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'account_accountant'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}