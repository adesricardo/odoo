# -*- coding: utf-8 -*-
{
    'name': "hnetw_employee_pos",

    'summary': """Empleados por comision""",

    'description': """
       Agrega un campo empleado comision a la linea de producto. tambien se agrega un campo de pos a los empleados.
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",


    'category': 'POS',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['hr', 'point_of_sale'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/hrEmployee.xml',
        'views/posOrderLine.xml',
    ],
}