# -*- coding: utf-8 -*-


from odoo import models, fields, api


class HrEmployee(models.Model):
    _inherit = 'hr.employee'
    _name = 'hr.employee'

    x_POS = fields.Many2one('pos.config', string='Punto de Venta')