# -*- coding: utf-8 -*-


from odoo import models, fields, api


class HrEmployee(models.Model):
    _inherit = 'pos.order.line'
    _name = 'pos.order.line'

    x_empleado = fields.Char(string='Empleado Comision')