# -*- coding: utf-8 -*-

from odoo import models, fields, api


class hrEmployee(models.Model):
    _inherit = 'hr.employee'

    Servicios = fields.Integer(string="Servicios")
    Productos = fields.Integer(string="Productos")
    Bebidas   = fields.Integer(string="Bebidas alcohólicas")

