# -*- coding: utf-8 -*-

from odoo import models, fields, api


def contar_veces(elemento, lista):
    veces = 0
    for i in lista:
        if elemento == i:
            veces += 1
    return veces


class PosOrderLine(models.Model):
     _name = 'pos.order.line'
     _inherit = 'pos.order.line'

     x_comision = fields.Float(string="Comision", compute="calculo")
     x_sesion = fields.Char(string="Sesión")
     x_referencia = fields.Char(string="Referencia")


     def calculo(self):
          arregloField = []
          arregloRepetido = []

          for variable in self:
             variable.x_sesion = (variable.order_id.session_id.name)
             variable.x_referencia =  (variable.order_id.name)
             if variable.x_empleado != False:
                 employee_ids = self.env['hr.employee'].search([('name', '=', variable.x_empleado)])
                 if employee_ids.name != False:
                     if variable.product_id.categ_id.hnetw_type_product == 'Servicios':
                         variable.x_comision = round((employee_ids.Servicios/100)*variable.price_subtotal, 2)
                     if variable.product_id.categ_id.hnetw_type_product == 'Bebidas':
                         variable.x_comision = round((employee_ids.Bebidas/100)*variable.price_subtotal, 2)
                     if variable.product_id.categ_id.hnetw_type_product == 'Productos':
                         variable.x_comision = round((employee_ids.Productos/100)*variable.price_subtotal, 2)

          pedido_ids =  self.env['pos.order'].search([('name', '!=', 'NULL')])
          for x in pedido_ids:
              arregloField.append(x.pos_reference)

          for x in arregloField:
              n = contar_veces(x, arregloField)
              if n > 1:
                 arregloRepetido.append(x)

          for variable in self:
              for  repeticion in arregloRepetido:
                 if variable.order_id.pos_reference == repeticion:
                    variable.x_comision = 0



