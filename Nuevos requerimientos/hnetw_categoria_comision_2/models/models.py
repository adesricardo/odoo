# -*- coding: utf-8 -*-

from odoo import models, fields, api


class ProductTemplate(models.Model):
    _inherit = 'product.category'

    hnetw_type_product = fields.Selection(
        [('Servicios', 'Servicios'), ('Productos', 'Productos'), ('Bebidas', 'Bebidas alcohólicas'),],
        string='Tipo de Categoria')

    ##hnetw_type_product = fields.Selection(selection=[('Servicios', 'Productos', 'Bebidas alcohólicas')], string="Tipo de Comision")
