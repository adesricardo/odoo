# -*- coding: utf-8 -*-

from odoo import models, fields, api

class ProductTemplate(models.Model):
     _inherit = 'product.template'

     numeracionCategoria = fields.Integer(string="Numeracion de Categoria", default=0, readonly=False)

