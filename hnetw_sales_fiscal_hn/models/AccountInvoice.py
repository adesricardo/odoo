# -*- coding: utf-8 -*-

from odoo import models, fields, api


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'
    _name = 'account.invoice'

    hnetw_fiscal_sales_cai = fields.Char(string="CAI", size=44)
    hnetw_fiscal_sales_nfactura = fields.Char(string="N° Factura Fiscal", size=19)