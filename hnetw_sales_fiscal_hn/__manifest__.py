# -*- coding: utf-8 -*-
{
    'name': "hnetw_sales_fiscal_hn",

    'summary': """
        Ampliación de campos en el modulo de ventas.
        """,

    'description': """
        Ampliación de campos en el modulo de ventas para poder registrar la información revelante al régimen fiscal hondureño
    """,

    'author': "HNETW",
    'website': "http://www.hnetw.com",


    'category': 'Sales',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'account'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/AccountInvoice.xml',
    ],
}