# -*- coding: utf-8 -*-

from odoo import models, fields, api

class PosConfig(models.Model):
     _inherit = 'pos.config'

     x_rangoInicial = fields.Char(string="Rango Incial")
     x_rangoFinal = fields.Char(string="Rango Final")
     x_fechaVigencia = fields.Date(string="Fecha de Vigencia")
     x_numeroDeFacturaActual  = fields.Char(string="Numero Actual")
     x_cai = fields.Char(string="CAI")

