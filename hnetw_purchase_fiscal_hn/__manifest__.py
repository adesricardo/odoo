# -*- coding: utf-8 -*-
{
    'name': "hnetw_purchase_fiscal_hn",

    'summary': """
          Ampliación de campos en el modulo de compra.
          """,

    'description': """
          Ampliación de campos en el modulo de compra para poder registrar la información revelante al régimen fiscal hondureño
      """,

    'author': "HNETW",
    'website': "http://www.hnetw.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Purchase',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'account'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/AccountInvoice.xml',
    ],
    "application": "True",
}