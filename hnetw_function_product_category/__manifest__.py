# -*- coding: utf-8 -*-
{
    'name': "hnetw_function_product_category",

    'summary': """
        Funcion de actualizacion de numeracion de productos""",

    'description': """
        Funcion de actualizacion para numeracion de productos
    """,

    'author': "HNETW",
    'website': "http://www.hnetw.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'product',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'product'],


}