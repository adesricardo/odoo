# -*- coding: utf-8 -*-

from odoo import models, fields, api


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    @api.onchange('categ_id')
    def agregarNumeros(self):
        record_ids = self.env['product.template'].search([('categ_id.name', '=', self.categ_id.name)])
        numeracion = []
        for variable in record_ids:
             numeracion.append(variable.numeracionCategoria)


        print(len(numeracion))
        if len(numeracion) != 0:
            mayor = numeracion[0]
            for mayores in numeracion:
                  if mayores > mayor:
                      mayor = mayores;

            if mayor < self.categ_id.rango_inicial:
                self.numeracionCategoria = self.categ_id.rango_inicial
            if mayor >= self.categ_id.rango_inicial:
                self.numeracionCategoria = mayor+1
            if mayor > self.categ_id.rango_final:
                self.agregarNumeros = 0
        if len(numeracion) == 0:
            self.numeracionCategoria = self.categ_id.rango_inicial
